﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SportsStore.WebUI.Models;
using SportsStore.WebUI.HtmlHelpers;
using SportsStore.WebUI.Controllers;
using SportsStore.Domain.Entities;
using SportsStore.Domain.Abstract;
using System.Web.Mvc;
using System.Web;
using System.Linq;
using Moq;
using System.Collections.Generic;

namespace SportsStore.UnitTests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void CanGenerate_Page_Links()
        {
            //przygotowanie 
            //aby użyć metody rozszerzającej

            HtmlHelper myhelper = null;

            //przygotowanie

            PagingInfo pagingInfo = new PagingInfo
            {
                CurrentPage = 2,
                TotalItems = 28,
                ItemsPerPage = 10
            };

            //przygotowanie - konfigrowanie deleganta z użyciem lambda
            Func<int, string> pageUrlDelegate = i => "Strona" + i;

            //działanie
            MvcHtmlString result = myhelper.PageLinks(pagingInfo, pageUrlDelegate);

            //asercje

            Assert.AreEqual(@"<a class=""btn btn-default"" href=""Strona1"">1</a>" + @"<a class=""btn btn-default btn-primary selected"" href=""Strona2"">2</a>" + @"<a class="" btn btn-default"" href=""Strona3"">3</a>", result.ToString());
        }

        [TestMethod]
        public void Can_Send_Pagination_View_Model()
        {

            //przygotowanie
            Mock<IProductRepository> mock = new Mock<IProductRepository>();
            mock.Setup(m => m.Products).Returns(new Product[]{
                new Product{ProductID =1, Name="P1"},
                new Product{ProductID =2, Name="P2"},
                new Product{ProductID =3, Name="P3"},
                new Product{ProductID =4, Name="P4"},
                new Product{ProductID =5, Name="P5"}
            });

            //przygotowanie
            ProductController controller = new ProductController(mock.Object);
            controller.PageSize = 3;

            //działanie
            ProductsListViewModel result = (ProductsListViewModel)controller.List(null,2).Model;

            //aserscje

            PagingInfo pageInfo = result.PagingInfo;
            Assert.AreEqual(pageInfo.CurrentPage, 2);
            Assert.AreEqual(pageInfo.ItemsPerPage, 3);
            Assert.AreEqual(pageInfo.TotalItems, 5);
            Assert.AreEqual(pageInfo.TotalPages, 2);
           // Product[] prodArray = result.Products.ToArray();
        }
        [TestMethod]
        public void Can_Add_To_Cart()
        {
            //prygotowanie
            Mock<IProductRepository> mock = new Mock<IProductRepository>();
            mock.Setup(x => x.Products).Returns(new Product[] { new Product { ProductID = 1, Name = "P1", Category = "Jab" }, }.AsQueryable());

            Cart cart = new Cart();

            CartController target = new CartController(mock.Object,null);

            //działanie

            target.AddToCart(cart, 1, null);

            //assercje

            Assert.AreEqual(cart.Lines.Count(), 1);

            Assert.AreEqual(cart.Lines.ToArray()[0].Product.ProductID, 1);

        }

        [TestMethod]
        public void Adding_Product_To_cart_goues_to_cart_screen()
        {
            //przygotownie

            Mock<IProductRepository> mock = new Mock<IProductRepository>();
            mock.Setup(x => x.Products).Returns(new Product[] { new Product { ProductID = 1, Name = "P1", Category = "Jabłka" }, }.AsQueryable());

            Cart cart = new Cart();

            CartController target = new CartController(mock.Object,null);

            //działanie

            RedirectToRouteResult result = target.AddToCart(cart, 2, "myUrl");

            //assercje

            Assert.AreEqual(result.RouteValues["action"], "Index");
            Assert.AreEqual(result.RouteValues["returnUrl"], "myUrl");

        }

        [TestMethod]
        public void Can_View_Cart_Contents()
        {
            //przygotowanie
            Cart cart = new Cart();

            CartController target = new CartController(null,null);

            //działanie
            CartIndexViewModel result = (CartIndexViewModel)target.Index(cart, "myUrl").ViewData.Model;

            //asercje

            Assert.AreSame(result.Cart, cart);
            Assert.AreEqual(result.ReturnUrl, "myUrl");
        }

        [TestMethod]
        public void Cannot_Empty_Cart()
        {
            Mock<IOrderProcessor> mock = new Mock<IOrderProcessor>();
            //przygotowanie
            Cart cart = new Cart();

            ShippingDetails shippingDetails = new ShippingDetails();

            CartController target = new CartController(null, mock.Object); 
            //działanie

            ViewResult result = target.Checkout(cart, shippingDetails);
            //asercje
            mock.Verify(m=>m.ProcessOrder(It.IsAny<Cart>(),It.IsAny<ShippingDetails>()),Times.Never());

            Assert.AreEqual("", result.ViewName);

            Assert.AreEqual(false, result.ViewData.ModelState.IsValid);
        }

        [TestMethod]
        public void Cannot_Checkout_Invalid_ShippingDetails()
        {
            Mock<IOrderProcessor> mock = new Mock<IOrderProcessor>();
            //przygotowanie
            Cart cart = new Cart();
            cart.AddItem(new Product(), 1);

            CartController target = new CartController(null, mock.Object);

            target.ModelState.AddModelError("error", "error");

            //działanie

            ViewResult result = target.Checkout(cart, new  ShippingDetails());

            //asercje
            mock.Verify(m => m.ProcessOrder(It.IsAny<Cart>(), It.IsAny<ShippingDetails>()), Times.Never());

            Assert.AreEqual("", result.ViewName);

            Assert.AreEqual(false, result.ViewData.ModelState.IsValid);
        }

        [TestMethod]
        public void Can_Checkout_And_Submit_Order()
        {
            //przygotowanie

            Mock<IOrderProcessor> mock = new Mock<IOrderProcessor>();
            
            Cart cart = new Cart();
            cart.AddItem(new Product(), 1);

            CartController target = new CartController(null, mock.Object);
            //działanie

            ViewResult result = target.Checkout(cart, new ShippingDetails());
            //asercje
            mock.Verify(m => m.ProcessOrder(It.IsAny<Cart>(), It.IsAny<ShippingDetails>()), Times.Once());

            Assert.AreEqual("Completed", result.ViewName);

            Assert.AreEqual(true, result.ViewData.ModelState.IsValid);
        }



    }

    [TestClass]
    public class AdminTests
    {
        [TestMethod]
        public void Index_Contains_All_Product()
        {
            //przygotowanie
            Mock<IProductRepository> mock = new Mock<IProductRepository>();

            mock.Setup(x => x.Products).Returns(new Product[] {
                new Product{ProductID=1, Name="P1"},
                new Product{ProductID=2, Name="P2"},
                new Product{ProductID=3, Name="P3"},
            });

            AdminController target = new AdminController(mock.Object);

            //działanie 

            //Product[] result = ((IEnumerable<Product>)target.Index().ViewData.Model).ToArray();

            //asercje

           // Assert.AreEqual(result.Length, 3);
           /// Assert.AreEqual("P1", result[0].Name);
            //Assert.AreEqual("P2", result[1].Name);
            //Assert.AreEqual("P3", result[2].Name);
        }
    }
}
