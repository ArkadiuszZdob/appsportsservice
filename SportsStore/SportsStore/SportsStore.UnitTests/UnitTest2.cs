﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SportsStore.WebUI.Models;
using SportsStore.WebUI.HtmlHelpers;
using SportsStore.WebUI.Controllers;
using SportsStore.Domain.Entities;
using SportsStore.Domain.Abstract;
using System.Web.Mvc;
using System.Web;
using System.Linq;
using Moq;
using System.Collections.Generic;

namespace SportsStore.UnitTests
{
    [TestClass]
    public class UnitTest2
    {
        [TestMethod]
        public void Can_Edit_Product()
        {
            //przygotowanie 

            Mock<IProductRepository> mock = new Mock<IProductRepository>();
            mock.Setup(m => m.Products).Returns(new Product[]{new Product{ProductID=1,Name="P1"},
                new Product{ProductID=2,Name="P2"},
                new Product{ProductID=3,Name="P3"},
                new Product{ProductID=4,Name="P4"},});

            AdminController target = new AdminController(mock.Object);

            //działanie

            Product p1 = target.Edit(1).ViewData.Model as Product;
            Product p2 = target.Edit(2).ViewData.Model as Product;
            Product p3 = target.Edit(3).ViewData.Model as Product;

            //asercje

            Assert.AreEqual(1, p1.ProductID);
            Assert.AreEqual(2, p2.ProductID);
            Assert.AreEqual(3, p3.ProductID);

        }

        [TestMethod]
        public void Cannot_Edit_Ninexistend_product()
        {

            //przygotowanie 

            Mock<IProductRepository> mock = new Mock<IProductRepository>();
            mock.Setup(m => m.Products).Returns(new Product[]{new Product{ProductID=1,Name="P1"},
                new Product{ProductID=2,Name="P2"},
                new Product{ProductID=3,Name="P3"},
                new Product{ProductID=4,Name="P4"},});

            AdminController target = new AdminController(mock.Object);

            //działanie
            Product result = (Product)target.Edit(4).ViewData.Model;

            //asercje

            Assert.IsNull(result);
        }
        [TestMethod]
        public void Can_Save_Valid_Changes()
        {
            Mock<IProductRepository> mock = new Mock<IProductRepository>();
            AdminController target = new AdminController(mock.Object);

            Product product = new Product { Name = "Test" };

            //działanie
            ActionResult result = target.Edit(product);

            //assercje 
            mock.Verify(m => m.SaveProduct(product));

            Assert.IsNotInstanceOfType(result, typeof(ViewResult));

        }

        [TestMethod]
        public void Cannot_save_Invalid_Changes()
        {
            Mock<IProductRepository> mock = new Mock<IProductRepository>();
            AdminController target = new AdminController(mock.Object);

            Product product = new Product { Name = "Test" };

            //dodanie błedu
            target.ModelState.AddModelError("error", "error");

            //próba zapisania produktu
            ActionResult result = target.Edit(product);

            //asercje-sprawdzenie czy nie zostało wywołane repozytorium
            mock.Verify(m => m.SaveProduct(It.IsAny<Product>()), Times.Never());

            //asercje sprawdzanie typu zwracanego z metody
            Assert.IsInstanceOfType(result, typeof(ViewResult));


        }

        [TestMethod]
        public void Delete_product()
        {
            Product prod = new Product { ProductID = 2, Name = "Test" };

            Mock<IProductRepository> mock = new Mock<IProductRepository>();

            mock.Setup(m => m.Products).Returns(new Product[]{new Product{ProductID=1, Name="P1"},
                prod,
                new Product{ProductID=3, Name="P3"}});

            AdminController target = new AdminController(mock.Object);

            target.Delete(prod.ProductID);

            //asercje

            mock.Verify(m => m.DeleteProduct(2));
        }
    }
}
