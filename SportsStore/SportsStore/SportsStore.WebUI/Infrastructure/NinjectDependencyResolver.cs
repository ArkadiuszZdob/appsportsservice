﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ninject;
using System.Web.Mvc;
using Moq;
using System.Configuration;
using SportsStore.Domain.Abstract;
using SportsStore.Domain.Entities;
using SportsStore.Domain.Concrete;
using SportsStore.WebUI.Infrastructure.Abstract;
using SportsStore.WebUI.Infrastructure.Concrete;

namespace SportsStore.WebUI.Infrastructure
{
    public class NinjectDependencyResolver : IDependencyResolver
    {
        private IKernel kernel;

        public NinjectDependencyResolver(IKernel kernelParam)
        {
            kernel = kernelParam;
            AddBindings();
        }

        public object GetService(Type serviceType)
        {
            return kernel.TryGet(serviceType);
        }
        public IEnumerable<object> GetServices(Type servicesType)
        {
            return kernel.GetAll(servicesType);
        }

        private void AddBindings()
        {
            //Mock<IProductRepository> mock = new Mock<IProductRepository>();
           // mock.Setup(m => m.Products).Returns(new List<Product>{
            //new Product{Name="Piłka Nożna", Price=25},
           // new Product{Name="Deska Surfingowa",Price=179},
           // new Product{Name="Buty do biegania", Price=95}});
            //kernel.Bind<IProductRepository>().ToConstant(mock.Object);
           kernel.Bind<IProductRepository>().To<EFProductRepository>();
           EmailSettings emailSettings = new EmailSettings
           {
               WriteAsFile = bool.Parse(ConfigurationManager.AppSettings["Email.WriteAsFile"] ?? "false")
           };
           kernel.Bind<IOrderProcessor>().To<EmailOrderProcessor>().WithConstructorArgument("settings", emailSettings);
           kernel.Bind<IAuthProvider>().To<FormsAuthProvider>();
        }
    }
}